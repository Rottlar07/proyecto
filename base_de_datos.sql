-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: bthb3floqqoye1dd6uax-mysql.services.clever-cloud.com:3306
-- Generation Time: Nov 24, 2021 at 03:41 AM
-- Server version: 8.0.22-13
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bthb3floqqoye1dd6uax`
--

-- --------------------------------------------------------

--
-- Table structure for table `talumno`
--

CREATE TABLE `talumno` (
  `CodAlumno` varchar(10) NOT NULL,
  `APaterno` varchar(100) DEFAULT NULL,
  `AMaterno` varchar(100) DEFAULT NULL,
  `Nombres` varchar(100) DEFAULT NULL,
  `Usuario` varchar(30) DEFAULT NULL,
  `CodCarrera` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `talumno`
--

INSERT INTO `talumno` (`CodAlumno`, `APaterno`, `AMaterno`, `Nombres`, `Usuario`, `CodCarrera`) VALUES
('A0001', 'Torres', 'Montes', 'Joselyn', 'U0001', 'C0002'),
('A0002', 'Salazar', 'Magariño', 'Jose', 'U0002', 'C0002'),
('A0003', 'Dominguez', 'Rodriguez', 'Sabrina', 'U0003', 'C0003'),
('A0004', 'Alveda', 'Villar', 'Mariela', 'U0004', 'C0004'),
('A0005', 'Hurtado', 'Espinoza', 'Monica', 'U0005', 'C0005'),
('A0006', 'Rojas', 'Molina', 'Oscar', 'U0006', 'C0006'),
('A0007', 'Alcala', 'Muñoz', 'Pedro', 'U0007', 'C0007'),
('A0008', 'Zamudio', 'Arriola', 'Mario', 'U0008', 'C0008'),
('A0009', 'Avila', 'Estrada', 'Alejandro', 'U0009', 'C0009'),
('A0010', 'Palomino', 'Jimenez', 'Micaela', 'U0010', 'C0010'),
('A0011', 'Gimenez', 'Lopez', 'Santiago', 'U0011', 'C0001'),
('A0012', 'Gomez', 'Silva', 'Luisa', 'U0012', 'C0001'),
('A0013', 'Carrasco', 'Solar', 'Nicolas', 'U0013', 'C0002'),
('A0014', 'Villar', 'Zamudio', 'Elías', 'U0014', 'C0003');

-- --------------------------------------------------------

--
-- Table structure for table `tasignatura`
--

CREATE TABLE `tasignatura` (
  `CodAsignatura` varchar(10) NOT NULL,
  `Asignatura` varchar(100) DEFAULT NULL,
  `CodRequisito` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `tasignatura`
--

INSERT INTO `tasignatura` (`CodAsignatura`, `Asignatura`, `CodRequisito`) VALUES
('AS001', 'Modelamiento de Base de datos ', 'AS001'),
('AS002', 'Pre Base de datos 1', 'AS001'),
('AS003', 'Base de datos 2', 'AS003'),
('AS004', 'Base de datos Avanzado', 'AS004'),
('AS005', 'Reparacion y mantemiento de Hardware', 'AS005'),
('AS006', 'Estructura de dastos I', 'AS006'),
('AS007', 'Desarrollo e implementacion de escritorio', 'AS007'),
('AS008', 'Sistemas operativos 1', 'AS008'),
('AS009', 'Sistemas operativos 2', 'AS009'),
('AS010', 'Lenguaje de programacion 1', 'AS010'),
('AS011', 'Lenguaje de programacion 2', 'AS011'),
('AS012', 'Diseño web', 'AS012'),
('AS013', 'Taller de Ofimatica', 'AS013'),
('AS014', 'Excel 1', 'AS014'),
('AS015', 'Excel 2', 'AS015'),
('AS016', 'Excel 3', 'AS016'),
('AS017', 'Administracion de Base de Datos', 'AS017'),
('AS018', 'Primeros Auxilios', 'AS018'),
('AS019', 'Matematica 1', 'AS019'),
('AS020', 'Matematica 2', 'AS020'),
('AS021', 'Hardware 1', 'AS021'),
('AS022', 'Hardware 2', 'AS022'),
('AS023', 'Cableado estructurado', 'AS023'),
('AS024', 'Modelamiento y diseño de sistemas', 'AS024'),
('AS025', 'Base de dastos I', 'AS025'),
('AS026', 'Base de dastos I', 'AS026'),
('AS027', 'Base de dastos I', 'AS027'),
('AS028', 'Base de dastos I', 'AS028'),
('AS029', 'Base de dastos I', 'AS029'),
('AS030', 'Base de dastos I', 'AS030'),
('AS031', 'Base de dastos I', 'AS031'),
('AS032', 'Base de dastos I', 'AS032'),
('AS033', 'Base de dastos I', 'AS033'),
('AS034', 'Base de dastos I', 'AS034'),
('AS035', 'Base de dastos I', 'AS035'),
('AS036', 'Base de dastos I', 'AS036'),
('AS037', 'Base de dastos I', 'AS037'),
('AS038', 'Base de dastos I', 'AS038'),
('AS039', 'Base de dastos I', 'AS039'),
('AS040', 'Base de dastos I', 'AS040'),
('AS041', 'Base de datos X', 'AS040'),
('AS042', 'Base de datos XI', 'AS041'),
('AS043', 'Base de datos XII', 'AS042'),
('AS044', 'Base de datos XIII', 'AS043');

-- --------------------------------------------------------

--
-- Table structure for table `tcarga`
--

CREATE TABLE `tcarga` (
  `IdCarga` varchar(10) NOT NULL,
  `CodDocente` varchar(10) DEFAULT NULL,
  `CodAsignatura` varchar(10) DEFAULT NULL,
  `Semestre` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `tcarga`
--

INSERT INTO `tcarga` (`IdCarga`, `CodDocente`, `CodAsignatura`, `Semestre`) VALUES
('1', 'D0001', 'AS001', '2021-1'),
('10', 'D0010', 'AS010', '2021-2'),
('11', 'D0011', 'AS011', '2021-1'),
('12', 'D0012', 'AS012', '2021-2'),
('13', 'D0013', 'AS013', '2021-1'),
('14', 'D0014', 'AS014', '2021-2'),
('15', 'D0015', 'AS015', '2021-1'),
('2', 'D0002', 'AS002', '2021-2'),
('3', 'D0003', 'AS003', '2021-1'),
('4', 'D0004', 'AS004', '2021-2'),
('5', 'D0005', 'AS005', '2021-1'),
('6', 'D0006', 'AS006', '2021-2'),
('7', 'D0007', 'AS007', '2021-1'),
('8', 'D0008', 'AS008', '2021-2'),
('9', 'D0009', 'AS009', '2021-1');

-- --------------------------------------------------------

--
-- Table structure for table `tcarrera`
--

CREATE TABLE `tcarrera` (
  `CodCarrera` varchar(10) NOT NULL,
  `Carrerar` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `tcarrera`
--

INSERT INTO `tcarrera` (`CodCarrera`, `Carrerar`) VALUES
('C0001', 'Sistemas'),
('C0002', 'Enfermeria'),
('C0003', 'Gatronomia'),
('C0004', 'Redes'),
('C0005', 'Diseño Grafico'),
('C0006', 'Diseño Web'),
('C0007', 'Contabilidad'),
('C0008', 'Administracion'),
('C0009', 'Secretariado'),
('C0010', 'Marketing');

-- --------------------------------------------------------

--
-- Table structure for table `tdocente`
--

CREATE TABLE `tdocente` (
  `CodDocente` varchar(10) NOT NULL,
  `APaterno` varchar(100) DEFAULT NULL,
  `AMaterno` varchar(100) DEFAULT NULL,
  `Nombres` varchar(100) DEFAULT NULL,
  `Usuario` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `tdocente`
--

INSERT INTO `tdocente` (`CodDocente`, `APaterno`, `AMaterno`, `Nombres`, `Usuario`) VALUES
('D0001', 'Salazar', 'Bautista', 'Sonia', 'UD0001'),
('D0002', 'Mendoza', 'Prada', 'Pamela', 'UD0002'),
('D0003', 'Aguilar', 'Carvajal ', 'Jorge', 'UD0003'),
('D0004', 'Barragán', 'Figueroa', 'Alonso', 'UD0004'),
('D0005', 'Garrido', 'Llano', 'Luis', 'UD0005'),
('D0006', 'Lara', 'Pardo', 'Leo', 'UD0006'),
('D0007', 'Madrigal', 'Oliver ', 'Arturo', 'UD0007'),
('D0008', 'Paredes ', 'Prada', 'Rodolfo', 'UD0008'),
('D0009', 'Vidal ', 'Pastor', 'Renzo', 'UD0009'),
('D0010', 'Zurita ', 'Prada', 'Miguel', 'UD0010'),
('D0011', 'Suárez ', 'Alcántara', 'Jhony', 'UD0011'),
('D0012', 'Romero', 'Ocaña ', 'Tiago', 'UD0012'),
('D0013', 'Segura', 'Toledo', 'Fernando', 'UD0013'),
('D0014', 'Taboada', 'Ocaña', 'Facundo', 'UD0014'),
('D0015', 'Salazar', 'Paz', 'Adriano', 'UD0015');

-- --------------------------------------------------------

--
-- Table structure for table `tnotas`
--

CREATE TABLE `tnotas` (
  `CodTNotas` varchar(10) NOT NULL,
  `CodAlumno` varchar(10) DEFAULT NULL,
  `CodAsignatura` varchar(10) DEFAULT NULL,
  `Semestre` varchar(10) DEFAULT NULL,
  `Parcial1` float DEFAULT NULL,
  `Parcial2` float DEFAULT NULL,
  `NotaFinal` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `tnotas`
--

INSERT INTO `tnotas` (`CodTNotas`, `CodAlumno`, `CodAsignatura`, `Semestre`, `Parcial1`, `Parcial2`, `NotaFinal`) VALUES
('N001', 'A0001', 'AS001', 'Primer Sem', 14.5, 15, 14.75),
('N002', 'A0002', 'AS002', 'Segundo Se', 14.3, 15.5, 14.9),
('N003', 'A0003', 'AS003', 'Primer Sem', 13.8, 16, 14.9),
('N004', 'A0004', 'AS004', 'Segundo Se', 10.9, 15, 12.95),
('N005', 'A0005', 'AS005', 'Primer Sem', 17.1, 14, 15.55),
('N006', 'A0006', 'AS006', 'Segundo Se', 19, 18, 18.5),
('N007', 'A0007', 'AS007', 'Primer Sem', 16, 17, 16.5),
('N008', 'A0008', 'AS008', 'Segundo Se', 17.3, 11, 14.15),
('N009', 'A0009', 'AS009', 'Primer Sem', 13, 15.3, 14.15),
('N010', 'A0010', 'AS010', 'Segundo Se', 11.5, 20, 15.75),
('N011', 'A0001', 'AS011', 'Primer Sem', 9, 14, 11.5),
('N012', 'A0002', 'AS012', 'Segundo Se', 18.2, 11, 14.6),
('N013', 'A0003', 'AS013', 'Primer Sem', 1, 15, 8),
('N014', 'A0004', 'AS014', 'Segundo Se', 14, 15.6, 14.8),
('N015', 'A0005', 'AS015', 'Primer Sem', 15, 15.8, 15.4),
('N016', 'A0006', 'AS016', 'Segundo Se', 13.9, 19, 16.45),
('N017', 'A0007', 'AS017', 'Primer Sem', 14, 15, 14.5),
('N018', 'A0008', 'AS018', 'Segundo Se', 14.3, 18, 16.5),
('N019', 'A0009', 'AS019', 'Primer Sem', 17, 8, 12.5),
('N020', 'A0010', 'AS020', 'Segundo Se', 19, 19, 19),
('N021', 'A0001', 'AS022', 'Primer Sem', 20, 9, 14.5),
('N022', 'A0002', 'AS023', 'Segundo Se', 19, 2, 10.5),
('N023', 'A0003', 'AS023', 'Primer Sem', 8, 16.7, 12.35),
('N024', 'A0004', 'AS024', 'Segundo Se', 8, 18.1, 13.05),
('N025', 'A0005', 'AS025', 'Primer Sem', 9, 15.5, 12.5),
('N026', 'A0006', 'AS026', 'Segundo Se', 14, 17.8, 15.9),
('N027', 'A0007', 'AS027', 'Primer Sem', 13, 15.5, 14.25),
('N028', 'A0008', 'AS028', 'Segundo Se', 14, 15.9, 14.95),
('N029', 'A0009', 'AS029', 'Primer Sem', 14, 19, 16.5),
('N030', 'A0010', 'AS030', 'Segundo Se', 3, 20, 11.5),
('N031', 'A0001', 'AS031', 'Primer Sem', 14.9, 9, 11.95),
('N032', 'A0002', 'AS032', 'Segundo Se', 19, 3, 11),
('N033', 'A0003', 'AS033', 'Primer Sem', 13, 19, 16),
('N034', 'A0004', 'AS034', 'Segundo Se', 13, 3, 8),
('N035', 'A0005', 'AS035', 'Primer Sem', 12, 16, 14),
('N036', 'A0006', 'AS036', 'Segundo Se', 11, 3, 7),
('N037', 'A0007', 'AS037', 'Primer Sem', 16, 3, 9.5),
('N038', 'A0008', 'AS038', 'Segundo Se', 5, 19, 12),
('N039', 'A0009', 'AS039', 'Primer Sem', 20, 13, 16.5),
('N040', 'A0010', 'AS040', 'Segundo Se', 19, 18, 18.5);

-- --------------------------------------------------------

--
-- Table structure for table `tusuario`
--

CREATE TABLE `tusuario` (
  `Usuario` varchar(30) NOT NULL,
  `constrasena` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `tusuario`
--

INSERT INTO `tusuario` (`Usuario`, `constrasena`) VALUES
('U0001', '123'),
('U0002', '234'),
('U0003', '345'),
('U0004', '456'),
('U0005', '567'),
('U0006', '678'),
('U0007', '789'),
('U0008', '891'),
('U0009', '912'),
('U0010', '101'),
('U0011', '102'),
('U0012', '103'),
('U0013', '104'),
('U0014', '105'),
('U0015', '106'),
('U0016', '107'),
('U0017', '108'),
('U0018', '109'),
('U0019', '110'),
('U0020', '111'),
('U0021', '112'),
('U0022', '113'),
('U0023', '114'),
('U0024', '115'),
('U0025', '116'),
('U0026', '117'),
('U0027', '118'),
('U0028', '119'),
('U0029', '120'),
('U0030', '121'),
('U0031', '122'),
('U0032', '123'),
('U0033', '124'),
('U0034', '125'),
('U0035', '126'),
('U0036', '127'),
('U0037', '128'),
('U0038', '129'),
('U0039', '130'),
('U0040', '131'),
('UD0001', '123'),
('UD0002', '234'),
('UD0003', '345'),
('UD0004', '456'),
('UD0005', '567'),
('UD0006', '678'),
('UD0007', '789'),
('UD0008', '891'),
('UD0009', '912'),
('UD0010', '101'),
('UD0011', '102'),
('UD0012', '103'),
('UD0013', '104'),
('UD0014', '105'),
('UD0015', '106');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `talumno`
--
ALTER TABLE `talumno`
  ADD PRIMARY KEY (`CodAlumno`),
  ADD KEY `Usuario_name` (`Usuario`),
  ADD KEY `Codcarrera_cod` (`CodCarrera`);

--
-- Indexes for table `tasignatura`
--
ALTER TABLE `tasignatura`
  ADD PRIMARY KEY (`CodAsignatura`),
  ADD KEY `req_codasig` (`CodRequisito`);

--
-- Indexes for table `tcarga`
--
ALTER TABLE `tcarga`
  ADD PRIMARY KEY (`IdCarga`),
  ADD KEY `Coddocente_carga` (`CodDocente`),
  ADD KEY `Codasignatura_carga` (`CodAsignatura`);

--
-- Indexes for table `tcarrera`
--
ALTER TABLE `tcarrera`
  ADD PRIMARY KEY (`CodCarrera`);

--
-- Indexes for table `tdocente`
--
ALTER TABLE `tdocente`
  ADD PRIMARY KEY (`CodDocente`),
  ADD KEY `Usuario_named` (`Usuario`);

--
-- Indexes for table `tnotas`
--
ALTER TABLE `tnotas`
  ADD PRIMARY KEY (`CodTNotas`),
  ADD KEY `CodAsignatura_Nota` (`CodAsignatura`),
  ADD KEY `CodAlumno_Nota` (`CodAlumno`);

--
-- Indexes for table `tusuario`
--
ALTER TABLE `tusuario`
  ADD PRIMARY KEY (`Usuario`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `talumno`
--
ALTER TABLE `talumno`
  ADD CONSTRAINT `Codcarrera_cod` FOREIGN KEY (`CodCarrera`) REFERENCES `tcarrera` (`CodCarrera`),
  ADD CONSTRAINT `Usuario_name` FOREIGN KEY (`Usuario`) REFERENCES `tusuario` (`Usuario`);

--
-- Constraints for table `tasignatura`
--
ALTER TABLE `tasignatura`
  ADD CONSTRAINT `req_codasig` FOREIGN KEY (`CodRequisito`) REFERENCES `tasignatura` (`CodAsignatura`);

--
-- Constraints for table `tcarga`
--
ALTER TABLE `tcarga`
  ADD CONSTRAINT `Codasignatura_carga` FOREIGN KEY (`CodAsignatura`) REFERENCES `tasignatura` (`CodAsignatura`),
  ADD CONSTRAINT `Coddocente_carga` FOREIGN KEY (`CodDocente`) REFERENCES `tdocente` (`CodDocente`);

--
-- Constraints for table `tdocente`
--
ALTER TABLE `tdocente`
  ADD CONSTRAINT `Usuario_named` FOREIGN KEY (`Usuario`) REFERENCES `tusuario` (`Usuario`);

--
-- Constraints for table `tnotas`
--
ALTER TABLE `tnotas`
  ADD CONSTRAINT `CodAlumno_Nota` FOREIGN KEY (`CodAlumno`) REFERENCES `talumno` (`CodAlumno`),
  ADD CONSTRAINT `CodAsignatura_Nota` FOREIGN KEY (`CodAsignatura`) REFERENCES `tasignatura` (`CodAsignatura`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
