﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Public/Site1.Master" AutoEventWireup="true" CodeBehind="PerfilProfesional.aspx.cs" Inherits="Sistema_Académico_Universidad_ASP.net.Public.PerfilProfesional" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3>PERFIL</h3>
    <h2>Ingresante</h2>
    <p>El aspirante a la Escuela Profesional de Ingeniería de Sistemas deberá contar con:</p>
    <ul>
        <li>Capacidad de desarrollar la inteligencia lógico matemática.</li>
        <li>Capacidad de identificar modelos, calcular, formular y verificar hipótesis, utilizando el método deductivo e inductivo y buen nivel de abstracción.</li>
        <li>Capacidad de desarrollar la inteligencia lingüística que es la capacidad de comprender el orden y el significado de las palabras en la lectura, entender, hablar, escuchar y escribir eficazmente.</li>
        <li>Capacidad de leer en inglés.</li>
        <li>Capacidad de adaptarse fácilmente a los cambios tecnológicos y a la innovación siendo indagador.</li>
        <li>Habilidades para comunicarse y tener disposición para trabajar en equipo.</li>
        <li>Cualidades como ser colaborativo y proactivo.</li>
        <li>Capacidad para ubicar y relacionar objetos o elementos en contexto.</li>
    </ul>
    <h3>PERFIL</h3>
    <h2>Profesional</h2>
    <p>El profesional de Ingeniería de Sistemas de la Universidad Andina del Cusco logrará:</p>
    <ul>
        <li>OE1. Dominar tecnologías de información y comunicación para responder a las exigencias del mercado global a través de la investigación y la innovación tecnológica.</li>
        <li>OE2. Liderar equipos multidisciplinarios a nivel regional, nacional e internacional para el desarrollo de proyectos de sistemas de información y tecnologías de información y comunicación, demostrando profesionalidad, conducta ética y practicando valores andinos.</li>
        <li>OE3. Administrar, gestionar y dirigir proyectos de sistemas de información que satisfagan y respondan a las necesidades organizacionales para el desarrollo sostenible de la sociedad.</li>
    </ul>
    <p>(Resolución Nº 193-2019-CFIA-UAC)</p>
    <h3>PERFIL</h3>
    <h2>Egresado</h2>
    <h3>RESULTADOS DEL ESTUDIANTE</h3>
    <p>El programa debe permitir que los estudiantes logren, al momento de la graduación:</p>
    <p>[a] Conocimientos de Computación: La capacidad de aplicar conocimientos de matemáticas, ciencias, computación y una especialidad de computación apropiados para los resultados del estudiante y la disciplina del programa.</p>
    <p>[b] Análisis de Problemas: La capacidad de identificar, formular, investigar literatura y resolver problemas complejos de computación y otras disciplinas relevantes en el dominio.</p>
    <p>[c] Diseño y Desarrollo de Soluciones: La capacidad de diseñar, implementar y evaluar soluciones a problemas complejos de computación y diseñar y evaluar sistemas, componentes o procesos que satisfacen las necesidades específicas.</p>
    <p>[d] Trabajo Individual y en Equipo: La capacidad de desenvolverse eficazmente como individuo, como miembro o líder de equipos diversos.</p>
    <p>[e] Comunicación: La capacidad de comunicarse eficazmente, de forma oral y escrita, en una variedad de contextos profesionales.</p>
    <p>[f] Profesionalismo y Sociedad: La capacidad de analizar el impacto local y global de la computación sobre las personas, las organizaciones y la sociedad.</p>
    <p>[g] Aprendizaje continuo: La capacidad de reconocer la necesidad del aprendizaje y el desarrollo profesional continuo.</p>
    <p>[h] Uso de Herramientas Modernas: La capacidad de crear, seleccionar, adaptar y aplicar técnicas, recursos y herramientas modernas para la práctica de la computación, con la comprensión de sus limitaciones.</p>
    <p>[i] La comprensión y la capacidad para apoyar el uso, ejecución y gestión de sistemas de información dentro de un entorno de aplicación.</p>
    <p>(Resolución Nº 185-2019-CFIA-UAC)

</p>
</asp:Content>
