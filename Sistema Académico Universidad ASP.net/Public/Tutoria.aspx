﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Public/Site1.Master" AutoEventWireup="true" CodeBehind="Tutoria.aspx.cs" Inherits="Sistema_Académico_Universidad_ASP.net.Public.Tutoria" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <p>
        La Universidad Andina del Cusco ha implementado los programas de tutoría, en cada escuela profesional, como un mecanismo para fortalecer los conocimientos y destrezas del estudiantado. La tutoría es un proceso de ayuda, acompañamiento y orientación académica y personal-social a los estudiantes, desde su ingreso hasta la culminación de sus estudios.

En la Escuela Profesional de Ingeniería de Sistemas, este acompañamiento y seguimiento estudiantil se desarrolla mediante acciones de orientación e información y bajo distintas modalidades (atención individual o colectiva), según la naturaleza de las necesidades y requerimientos del estudiante o del grupo por atender, a través de los recursos humanos y tecnológicos con los que cuenta la UAC.

La tutoría es atendida por docentes tutores académicos, docentes tutores de formación, docentes tutores de inserción laboral – coordinadores del ejercicio pre-profesional, y pares tutores, quienes se encargan de guiar, orientar, acompañar y hacer el seguimiento al desempeño académico del estudiante.

Como parte del Programa de Tutoría establecido por la Facultad de Ingeniería y Arquitectura, se tiene un plan de acompañamiento y consejería en 3 niveles:
    </p>
    
    <ul>
        <li>Ingresantes</li>
        <li>Estudiantes Regulares</li>
        <li>Egresantes</li>
    </ul>
    
    <p>
        Para mayor información sobre la Tutoría en el programa de Ingeniería de Sistemas, puede contactar con los docentes tutores:
    </p>
    <ul>
        <li>Mtra. Yessenia Bernales Guzman / correo: ybernales@uandina.edu.pe</li>
        <li>Ing Velia Ardiles Romero / correo: vardiles@uandina.edu.pe</li>
        <li>Mg. Ing. Vivian Luz De la Vega Bellido / correo: vdelavega@uandina.edu.pe</li>
    </ul>
    
</asp:Content>
