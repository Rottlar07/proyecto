﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Public/Site1.Master" AutoEventWireup="true" CodeBehind="Procesos.aspx.cs" Inherits="Sistema_Académico_Universidad_ASP.net.Public.Procesos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <p>
        A continuación detallamos los procesos de Bachillerato y Titulación de la Escuela Profesional de Ingeniería de Sistemas como parte esencial de los trámites que realicen los estudiantes de nuestra escuela profesional.
    </p>
    <h3>Importante</h3>
    <p>Clic sobre cada formato para descargar el archivo</p>
    <h2>PROCESOS</h2>
    <ul>
        <li><a href="#">ESQUEMAS DE PROYECTOS DE TESIS Y GUÍA PARA SU DESARROLLO, PARA LA ESCUELA PROFESIONAL DE INGENIERÍA DE SISTEMAS</a></li>
        <li><a href="#">PROCESO PARA LA OBTENCIÓN DEL GRADO DE BACHILLER</a></li>
    </ul>
</asp:Content>
