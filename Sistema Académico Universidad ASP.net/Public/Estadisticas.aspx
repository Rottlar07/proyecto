﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Public/Site1.Master" AutoEventWireup="true" CodeBehind="Estadisticas.aspx.cs" Inherits="Sistema_Académico_Universidad_ASP.net.Public.WebForm2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 265px;
        }
        .auto-style2 {
            width: 311px;
        }
        .auto-style3 {
            width: 451px;
        }
        .auto-style4 {
            width: 265px;
            height: 27px;
        }
        .auto-style5 {
            width: 311px;
            height: 27px;
        }
        .auto-style6 {
            width: 451px;
            height: 27px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3>AÑO 2020</h3>
    <table style="width: 100%;border-color:black" border="1">
        <tr>
            <td class="auto-style1">SEMESTRE ACADÉMICO</td>
            <td class="auto-style2">N° ALUMNOS MATRICULADOS</td>
            <td class="auto-style3">N° ALUMNOS EGRESADOS</td>
            <td class="auto-style4">N° ALUMNOS GRADUADOS</td>
        </tr>
        <tr>
            <td class="auto-style1">2020-1</td>
            <td class="auto-style2">500</td>
            <td class="auto-style3">11</td>
            <td class="auto-style4">14</td>
        </tr>
        <tr>
            <td class="auto-style5">2020-2</td>
            <td class="auto-style6">529</td>
            <td class="auto-style7">7</td>
            <td class="auto-style8">8</td>

        </tr>
    </table>
    <h3>AÑO 2019</h3>
    <table style="width: 100%;" border="1">
        <tr>
            <td class="auto-style1">SEMESTRE ACADÉMICO</td>
            <td class="auto-style2">N° ALUMNOS MATRICULADOS</td>
            <td class="auto-style3">N° ALUMNOS EGRESADOS</td>
            <td class="auto-style4">N° ALUMNOS GRADUADOS</td>
        </tr>
        <tr>
            <td class="auto-style1">2020-1</td>
            <td class="auto-style2">500</td>
            <td class="auto-style3">11</td>
            <td class="auto-style4">14</td>
        </tr>
        <tr>
            <td class="auto-style5">2020-2</td>
            <td class="auto-style6">529</td>
            <td class="auto-style7">7</td>
            <td class="auto-style8">8</td>

        </tr>
    </table>
    <h3>AÑO 2018</h3>
    <table style="width: 100%;" border="1">
        <tr>
            <td class="auto-style1">SEMESTRE ACADÉMICO</td>
            <td class="auto-style2">N° ALUMNOS MATRICULADOS</td>
            <td class="auto-style3">N° ALUMNOS EGRESADOS</td>
            <td class="auto-style4">N° ALUMNOS GRADUADOS</td>
        </tr>
        <tr>
            <td class="auto-style1">2020-1</td>
            <td class="auto-style2">500</td>
            <td class="auto-style3">11</td>
            <td class="auto-style4">14</td>
        </tr>
        <tr>
            <td class="auto-style5">2020-2</td>
            <td class="auto-style6">529</td>
            <td class="auto-style7">7</td>
            <td class="auto-style8">8</td>

        </tr>
    </table>
    <h3>AÑO 2017</h3>
    <table style="width: 100%;" border="1">
        <tr>
            <td class="auto-style1">SEMESTRE ACADÉMICO</td>
            <td class="auto-style2">N° ALUMNOS MATRICULADOS</td>
            <td class="auto-style3">N° ALUMNOS EGRESADOS</td>
            <td class="auto-style4">N° ALUMNOS GRADUADOS</td>
        </tr>
        <tr>
            <td class="auto-style1">2020-1</td>
            <td class="auto-style2">500</td>
            <td class="auto-style3">11</td>
            <td class="auto-style4">14</td>
        </tr>
        <tr>
            <td class="auto-style5">2020-2</td>
            <td class="auto-style6">529</td>
            <td class="auto-style7">7</td>
            <td class="auto-style8">8</td>

        </tr>
    </table>
    <h3>AÑO 2016</h3>
    <table style="width: 100%;" border="1">
        <tr>
            <td class="auto-style1">SEMESTRE ACADÉMICO</td>
            <td class="auto-style2">N° ALUMNOS MATRICULADOS</td>
            <td class="auto-style3">N° ALUMNOS EGRESADOS</td>
            <td class="auto-style4">N° ALUMNOS GRADUADOS</td>
        </tr>
        <tr>
            <td class="auto-style1">2020-1</td>
            <td class="auto-style2">500</td>
            <td class="auto-style3">11</td>
            <td class="auto-style4">14</td>
        </tr>
        <tr>
            <td class="auto-style5">2020-2</td>
            <td class="auto-style6">529</td>
            <td class="auto-style7">7</td>
            <td class="auto-style8">8</td>

        </tr>
    </table>
</asp:Content>
