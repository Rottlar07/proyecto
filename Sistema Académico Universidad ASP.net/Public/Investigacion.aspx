﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Public/Site1.Master" AutoEventWireup="true" CodeBehind="Investigacion.aspx.cs" Inherits="Sistema_Académico_Universidad_ASP.net.Public.Investigacion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3>LINEAS DE INVESTIGACIÓN</h3>
    <p>Las líneas de investigación de la Escuela Profesional están enmarcadas en tres las áreas de dominio del conocimiento, y comprenden a su vez un conjunto de temas de interés. (Res. 373-2018-CFIA-UAC)</p>
    <h2>ÁREA DE DOMINIO: TECNOLOGÍAS DE INFORMACIÓN</h2>
    <h3>1. DESARROLLO DE SOFTWARE</h3>
    <ul>
        <li>Automatización de procesos.</li>
        <li>Desarrollo de aplicaciones usando inteligencia artificial.</li>
        <li>Realidad virtual.</li>
        <li>Sistemas de información geográfica.</li>
        <li>Investigación asistida por computadora.</li>
    </ul>
    <h3>2. CIENCIA DE DATOS</h3>
    <ul>
        <li>Modelación y simulación usando modelos matemáticos</li>
        <li>Inteligencia de negocios</li>
        <li>Tratamiento de grandes cantidades de datos.</li>
        <li>Seguridad de información.</li>
        <li>Almacenamiento y recuperación de la información.</li>
        <li>Machine Learning.</li>
    </ul>
    <h3>3. ALGORÍTMICA</h3>
    <ul>
        <li>Algoritmos de migración.</li>
        <li>Algoritmos numéricos.</li>
        <li>Biotecnología.</li>
        <li>Matemática aplicada computacional.</li>
        <li>Técnica algorítmica de juegos.</li>
    </ul>

    <h2>ÁREA DE DOMINIO: TECNOLOGÍAS DE COMUNICACIÓN</h2>
    <h3>1. COMUNICACIONES UNIFICADAS</h3>
    <ul>
        <li>Contenedores</li>
        <li>Virtualización</li>
        <li>Data center</li>
        <li>Protocolos de comunicación</li>
        <li>Normatividad</li>
        <li>Servicios de red</li>
        <li>Clustering</li>
        <li>Infraestructura</li>
    </ul>
    <h3>2. SEGURIDAD EN TECNOLOGÍAS DE INFORMACIÓN Y COMUNICACIÓN</h3>
    <ul>
        <li>Seguridad de redes</li>
        <li>Normativas</li>
    </ul>
    <h3>3. SISTEMAS DE CONTROL Y MONITOREO</h3>
    <ul>
        <li>Sistemas embebidos</li>
        <li>Scada</li>
        <li>Domótica</li>
    </ul>

    <h2>ÁREA DE DOMINIO: ORGANIZACIÓN EMPRESARIAL Y GESTIÓN DE INFORMACIÓN</h2>
    <h3>1. SISTEMAS DE INFORMACIÓN</h3>
    <ul>
        <li>Gestión de servicios de TI (ITIL, COBIT)</li>
        <li>Gestión de infraestructura de TI</li>
        <li>Gestión estratégica de TI (gestión de innovación tecnológica, estándares)</li>
        <li>Gestión estratégica de información.</li>
        <li>Gestión de proyectos en sistemas de información, para la gestión pública y privada.</li>
        <li>Auditoría de sistemas de información.</li>
        <li>Gestión de seguridad de sistemas de información.</li>
        <li>Gestión de procesos de negocio.</li>
        <li>Gestión de conocimiento.</li>
        <li>Gestión del cambio organizacional.</li>
        <li>Gestión de información (centros de investigación, publicaciones, bibliotecas, workflow de información).</li>
        <li>Inteligencia de negocios.</li>
        <li>Modelado y simulación de sistemas.</li>
        <li>Inteligencia artificial (redes neuronales).</li>
        <li>Calidad de sistemas de información.</li>
        <li>Metodologías del ciclo de vida de desarrollo de sistemas de información.</li>
    </ul>
    <h3>2. SOCIEDAD DE INFORMACIÓN</h3>
    <ul>
        <li>Alfabetización digital.</li>
        <li>Inclusión digital.</li>
        <li>Gobierno electrónico.</li>
        <li>E-learning.</li>
        <li>ESSALUD.</li>
        <li>E-agricultura.</li>
        <li>E-commerce.</li>
        <li>Sistemas de información geográficos.</li>
        <li>Marketing digital.</li>
    </ul>

</asp:Content>
