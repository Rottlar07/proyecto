﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Public/Site1.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Sistema_Académico_Universidad_ASP.net.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3>FECHA DE ANIVERSARIO</h3>
    <p>16 de agosto</p>
    <p>La Escuela Profesional de Ingeniería de Sistemas tiene más de 26 años de experiencia formando ingenieros capaces de diseñar soluciones para una amplia gama de problemas en el sector de la ingeniería y de otras disciplinas, mediante la aplicación del pensamiento sistémico y el uso de las tecnologías de la información y comunicación.

El graduado de la Escuela Profesional de Ingeniería de Sistemas está preparado para analizar, comprender, evaluar, seleccionar y optimizar, como un todo integrado, el funcionamiento de la estructura organizacional en una empresa. Asimismo, está capacitado para transformar una necesidad operativa en un sistema diseñado para satisfacer esa necesidad, a través de procesos iterativos de definición de sistemas, de síntesis del problema y de análisis, diseño y evaluación de las soluciones.

El ingeniero de sistemas de la Universidad Andina del Cusco domina las tecnologías de información, tecnologías de comunicación, modelación y simulación de sistemas. Además, conoce normas y estándares de calidad para la gestión de sistemas de información, gestión de TI, seguridad informática, ciberseguridad, desarrollo de software, entre otras. De igual forma, conceptúa y maneja la teoría de investigación, aplica métodos, técnicas e instrumentos de investigación científica.
    </p>
    <h2>Desempeño laboral</h2>
    <p>El Ingeniero de Sistemas se desempeña de manera solvente en todos los sectores económicos; en organizaciones de tipo privado, público o mixto; en el entorno regional, nacional, e internacional; en aquellas áreas donde sea necesaria la aplicación eficiente o el desarrollo especializado de tecnologías de información y comunicación para una adecuada gestión de la información. Además, su espíritu emprendedor e innovador posibilita que pueda generar empresas y productos tecnológicos. Lidera y se integra en equipos multidisciplinarios, gerencia proyectos de desarrollo y mantenimiento de software.</p>
    <h2>Acreditacion</h2>
    <p>El programa de Ingeniería de Sistemas de la Universidad Andina del Cusco está acreditado por el Comité de Acreditación de Computación de ICACIT
http://www.icacit.org.pe</p>
    <img src="https://www.uandina.edu.pe/wp-content/uploads/2021/05/icacit-300x93.jpg" alt="Alternate Text" />
</asp:Content>
