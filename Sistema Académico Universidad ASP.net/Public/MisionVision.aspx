﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Public/Site1.Master" AutoEventWireup="true" CodeBehind="MisionVision.aspx.cs" Inherits="Sistema_Académico_Universidad_ASP.net.Public.MisionVision" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3>NUESTRA</h3>
    <h2>Misión</h2>
    <p>Somos una escuela profesional que forma, de manera integral y con calidad, ingenieros de sistemas, a través del fortalecimiento de habilidades y capacidades para dominar tecnologías de la información, tecnologías de comunicación y la gestión y desarrollo de sistemas de información, con principios éticos profesionales y valores andinos, liderando equipos de trabajo, promotora de la investigación científica, el cuidado del ambiente, capaz de satisfacer y responder a las necesidades para el desarrollo sostenible de la sociedad a nivel regional y nacional.</p>
    <h3>NUESTRA</h3>
    <h2>Visión</h2>
    <p>La escuela profesional de Ingeniería de Sistemas de la Universidad Andina del Cusco, al 2025 será líder en la formación integral de ingenieros de sistemas, con una educación de calidad basada en la ciencia y tecnología, con proyección social, orientada a la gestión y desarrollo de sistemas de información, tecnologías de información y comunicación, con principios éticos profesionales y valores andinos de sabiduría (Yachay), trabajo (Llank’ay), voluntad (Munay), reciprocidad y solidaridad (Ayni), promoviendo la investigación, la conservación del ambiente, la cultura andina y el desarrollo sostenible de la sociedad a nivel regional y nacional.</p>
    <h3>ACONTECIMIENTOS IMPORTANTES</h3>
    <h2>Historia</h2>
    <p>La Escuela Profesional de Ingeniería de Sistemas (EPIS) inició su funcionamiento en el año 1993 con la denominación de Carrera Profesional de Ingeniería de Sistemas. Actualmente es parte de la estructura orgánica de la Facultad de Ingeniería y Arquitectura de la Universidad Andina del Cusco y cuenta con una plana docente de 34 profesionales.

Durante más de 26 años, la Escuela ha brindado servicio a un promedio de 450 estudiantes por ciclo académico, el cual tiene una duración de 17 semanas. Cuenta con más de 800 bachilleres, de los cuales 432 han obtenido el título profesional, que se encuentran laborando dentro y fuera de nuestra región.

La estructura curricular ha sido actualizada en varias ocasiones con la finalidad de estar acorde al avance de la tecnología, tendencias del sector y mercado laboral. En el año 2005 fue aprobada la segunda estructura curricular y, posteriormente, en el año 2013, se aprobó una tercera estructura. La malla curricular vigente es la aprobada en el 2016, y se encuentra en implementación el Plan de estudios 2020, cuyo primer ciclo ha entrado en vigencia en el semestre 2020 – I. Estos cambios obedecen a requerimientos señalados por nuestros egresados, empleadores y organismos nacionales e internacionales.</p>
</asp:Content>
