﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Public/Site1.Master" AutoEventWireup="true" CodeBehind="Docentes.aspx.cs" Inherits="Sistema_Académico_Universidad_ASP.net.Public.WebForm4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3>DIRECTOR</h3>
    <h2>Escuela Profesional</h2>
    <p>Dr. Ing. Lornel Antonio Rivas Mago

lrivas@uandina.edu.pe
    </p>
    <figure class="wpb_wrapper vc_figure">
		<a data-rel="prettyPhoto[rel-4745-3569405416]" href="https://www.uandina.edu.pe/wp-content/uploads/2021/02/is-lormel-rivas.jpg" target="_self" class="vc_single_image-wrapper vc_box_shadow_border_circle  vc_box_border_grey prettyphoto"><img width="270" height="300" src="https://www.uandina.edu.pe/wp-content/uploads/2021/02/is-lormel-rivas-270x300.jpg" class="vc_single_image-img attachment-medium" alt="" srcset="https://www.uandina.edu.pe/wp-content/uploads/2021/02/is-lormel-rivas-270x300.jpg 270w, https://www.uandina.edu.pe/wp-content/uploads/2021/02/is-lormel-rivas-768x853.jpg 768w, https://www.uandina.edu.pe/wp-content/uploads/2021/02/is-lormel-rivas.jpg 881w" sizes="(max-width: 270px) 100vw, 270px"></a>
	</figure>
    <figure class="wpb_wrapper vc_figure">
		<a href="https://www.uandina.edu.pe/descargas/transparencia/cv/002196697.pdf" target="_blank" class="vc_single_image-wrapper   vc_box_border_grey"><img width="150" height="150" src="https://www.uandina.edu.pe/wp-content/uploads/2021/04/icon-cv-150x150.png" class="vc_single_image-img attachment-thumbnail" alt="" srcset="https://www.uandina.edu.pe/wp-content/uploads/2021/04/icon-cv-150x150.png 150w, https://www.uandina.edu.pe/wp-content/uploads/2021/04/icon-cv.png 300w, https://www.uandina.edu.pe/wp-content/uploads/2021/04/icon-cv-120x120.png 120w" sizes="(max-width: 150px) 100vw, 150px"></a>
	</figure>
    <h3>DIRECTORA</h3>
    <h2>Escuela Profesional</h2>
    <p>Mgt. Ariadna Palomino Cahuaya

apalominoc@uandina.edu.pe
    </p>
    <figure class="wpb_wrapper vc_figure">
		<a data-rel="prettyPhoto[rel-4745-2172131392]" href="https://www.uandina.edu.pe/wp-content/uploads/2021/09/b.jpg" target="_self" class="vc_single_image-wrapper vc_box_shadow_border_circle  vc_box_border_grey prettyphoto"><img width="300" height="284" src="https://www.uandina.edu.pe/wp-content/uploads/2021/09/b-300x284.jpg" class="vc_single_image-img attachment-medium" alt="" srcset="https://www.uandina.edu.pe/wp-content/uploads/2021/09/b-300x284.jpg 300w, https://www.uandina.edu.pe/wp-content/uploads/2021/09/b.jpg 370w" sizes="(max-width: 300px) 100vw, 300px"></a>
	</figure>
    <figure class="wpb_wrapper vc_figure">
		<a href="https://www.uandina.edu.pe/descargas/transparencia/cv/29715419.pdf" target="_blank" class="vc_single_image-wrapper   vc_box_border_grey"><img width="150" height="150" src="https://www.uandina.edu.pe/wp-content/uploads/2021/04/icon-cv-150x150.png" class="vc_single_image-img attachment-thumbnail" alt="" srcset="https://www.uandina.edu.pe/wp-content/uploads/2021/04/icon-cv-150x150.png 150w, https://www.uandina.edu.pe/wp-content/uploads/2021/04/icon-cv.png 300w, https://www.uandina.edu.pe/wp-content/uploads/2021/04/icon-cv-120x120.png 120w" sizes="(max-width: 150px) 100vw, 150px"></a>
	</figure>
    <h3>ESCUELA PROFESIONAL DE INGENIERÍA DE SISTEMAS</h3>
    <h2>Plana Docente</h2>
    <p>La escuela cuenta con la plana docente de acuerdo al número de estudiantes matriculados coherente con el propósito y objetivos educacionales, calificados en las áreas del conocimiento profesional de la especialidad, sobre investigación, con experiencia en el campo profesional y la docencia, con una conducta ética consecuente con los valores y principios establecidos por la universidad y por la Escuela Profesional, tal como establece la Ley Universitaria, el Estatuto, los reglamentos, normas y directivas otorgados por la universidad, facultad o la Escuela que constituyen el marco normativo que orienta la gestión docente, cautelando el cumplimiento de sus deberes y derechos en el ámbito del quehacer académico y administrativo. Los docentes cuentan con grados académicos exigidos y con las calificaciones tanto profesionales como didácticas y personales que aseguran el logro del perfil de egreso de los futuros ingenieros de sistemas.
    </p>
    <a class="prettyphoto" href="https://www.uandina.edu.pe/wp-content/uploads/2021/02/plana-docente-sistemas-1024x594.jpg" data-rel="prettyPhoto[rel-4745-4059233425]"><img width="1024" height="594" src="https://www.uandina.edu.pe/wp-content/uploads/2021/02/plana-docente-sistemas-1024x594.jpg" class="attachment-large" alt="" srcset="https://www.uandina.edu.pe/wp-content/uploads/2021/02/plana-docente-sistemas-1024x594.jpg 1024w, https://www.uandina.edu.pe/wp-content/uploads/2021/02/plana-docente-sistemas-300x174.jpg 300w, https://www.uandina.edu.pe/wp-content/uploads/2021/02/plana-docente-sistemas-768x445.jpg 768w, https://www.uandina.edu.pe/wp-content/uploads/2021/02/plana-docente-sistemas-1536x890.jpg 1536w, https://www.uandina.edu.pe/wp-content/uploads/2021/02/plana-docente-sistemas-1080x626.jpg 1080w, https://www.uandina.edu.pe/wp-content/uploads/2021/02/plana-docente-sistemas.jpg 1920w" sizes="(max-width: 1024px) 100vw, 1024px"></a>
    
	<h2>Docentes Ordinarizados</h2>
    <ul>
        <li><a href="https://www.uandina.edu.pe/descargas/transparencia/cv/29715419.pdf">ACURIO GUTIERREZ, MARIA ISABEL</a></li>
        <li><a href="https://www.uandina.edu.pe/descargas/transparencia/cv/23920560.pdf">GANVINI VALCARCEL, CRISTHIAN EDUARDO</a></li>
        <li><a href="https://www.uandina.edu.pe/descargas/transparencia/cv/06268370.pdf">PALOMINO CAHUAYA, ARIADNA</a></li>
        <li><a href="https://www.uandina.edu.pe/descargas/transparencia/cv/24001157.pdf">CARRASCO POBLETE, EDWIN</a></li>
        <li><a href="https://www.uandina.edu.pe/descargas/transparencia/cv/24006964.pdf">MARCA AIMA, MONICA</a></li>
        <li><a href="https://www.uandina.edu.pe/descargas/transparencia/cv/23860669.pdf">PALOMINO OLIVERA, EMILIO</a></li>
        <li><a href="https://www.uandina.edu.pe/descargas/transparencia/cv/23872532.pdf">GAMARRA SALDIVAR, ENRIQUE</a></li>
    </ul>
    
	<h2>Jefes de práctica</h2>
    <ul>
        <li><a href="https://www.uandina.edu.pe/descargas/transparencia/cv/29715419.pdf">HUAMAN ATAULLUCO, FELIX ENRIQUE</a></li>
        <li><a href="https://www.uandina.edu.pe/descargas/transparencia/cv/23920560.pdf">POCCORI UMERES, GODOFREDO</a></li>
    </ul>
	
	<h2>Contratados</h2>
    <ul>
        <li><a href="https://www.uandina.edu.pe/descargas/transparencia/cv/29715419.pdf">ABE MIGUITA, PEDRO ENRIQUE</a></li>
        <li><a href="https://www.uandina.edu.pe/descargas/transparencia/cv/23920560.pdf">DEL CARPIO CUENTAS, LUIS ENRIQUE</a></li>
        <li><a href="https://www.uandina.edu.pe/descargas/transparencia/cv/06268370.pdf">NUÑEZ PACHECO, MARUJA</a></li>
        <li><a href="https://www.uandina.edu.pe/descargas/transparencia/cv/24001157.pdf">ALCCA ZELA, ERICK</a></li>
        <li><a href="https://www.uandina.edu.pe/descargas/transparencia/cv/24006964.pdf">ESPETIA HUAMANGA, HUGO</a></li>
        <li><a href="https://www.uandina.edu.pe/descargas/transparencia/cv/23860669.pdf">RAMIREZ VARGAS, ADRIEL</a></li>
        <li><a href="https://www.uandina.edu.pe/descargas/transparencia/cv/23872532.pdf">ARDILES ROMERO, VELIA</a></li>
        <li><a href="https://www.uandina.edu.pe/descargas/transparencia/cv/29715419.pdf">GONZALES CONDORI, HARRY YEISON</a></li>
        <li><a href="https://www.uandina.edu.pe/descargas/transparencia/cv/23920560.pdf">RIVAS MAGO, LORNEL ANTONIO</a></li>
        <li><a href="https://www.uandina.edu.pe/descargas/transparencia/cv/06268370.pdf">BERNALES GUZMAN, YESSENIA</a></li>
        <li><a href="https://www.uandina.edu.pe/descargas/transparencia/cv/24001157.pdf">HOLGUIN HERRERA, MELISA BETYS</a></li>
        <li><a href="https://www.uandina.edu.pe/descargas/transparencia/cv/24006964.pdf">SOTA ORELLANA, LUIS ALBERTO</a></li>
        <li><a href="https://www.uandina.edu.pe/descargas/transparencia/cv/23860669.pdf">CHAVEZ ESPINOZA, WILLIAM ALBERTO</a></li>
        <li><a href="https://www.uandina.edu.pe/descargas/transparencia/cv/23872532.pdf">LEON NUÑEZ, LIDA</a></li>
        <li><a href="https://www.uandina.edu.pe/descargas/transparencia/cv/29715419.pdf">VARGAS VERA, LIZET</a></li>
        <li><a href="https://www.uandina.edu.pe/descargas/transparencia/cv/23920560.pdf">CHOQUE SOTO, VANESSA MARIBEL</a></li>
        <li><a href="https://www.uandina.edu.pe/descargas/transparencia/cv/06268370.pdf">MOLERO DELGADO, IVAN</a></li>
        <li><a href="https://www.uandina.edu.pe/descargas/transparencia/cv/24001157.pdf">VILLENA LEON, OLMER CLAUDIO</a></li>
        <li><a href="https://www.uandina.edu.pe/descargas/transparencia/cv/24006964.pdf">CUBA DEL CASTILLO, MARIA YORNET</a></li>
        <li><a href="https://www.uandina.edu.pe/descargas/transparencia/cv/23860669.pdf">MOREANO CORDOVA, JAVIER</a></li>
        <li><a href="https://www.uandina.edu.pe/descargas/transparencia/cv/23872532.pdf">ZAMBRANO INCHAUSTEGUI, CARLOS ALBERTO</a></li>
        <li><a href="https://www.uandina.edu.pe/descargas/transparencia/cv/23872532.pdf">DE LA VEGA BELLIDO, VIVIAN LUZ</a></li>
        <li><a href="https://www.uandina.edu.pe/descargas/transparencia/cv/23872532.pdf">MUÑOZ YEPEZ, JOSE LUIS</a></li>
    </ul>
	



</asp:Content>
