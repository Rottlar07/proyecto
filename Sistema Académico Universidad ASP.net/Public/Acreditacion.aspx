﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Public/Site1.Master" AutoEventWireup="true" CodeBehind="Acreditacion.aspx.cs" Inherits="Sistema_Académico_Universidad_ASP.net.Public.Acreditacion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3>DOCUMENTOS</h3>
    <h3>ESCUELA PROFESIONAL</h3>
    <h2>Ingeniería de Sistemas</h2>
    <p>ACREDITACIÓN RIEV: 03 años</p>
    <aside><img src="https://www.uandina.edu.pe/wp-content/uploads/2021/01/fia-acreditacion-sistemas-02-200x300.jpg" alt="Alternate Text" /></aside>
    <h3>ESCUELA PROFESIONAL</h3>
    <h2>Ingeniería de Sistemas</h2>
    <p>ACREDITACIÓN ICACIT: 06 años</p>
    <aside><img src="https://www.uandina.edu.pe/wp-content/uploads/2021/02/fia-acreditacion-sistemas-02-300x225.jpg" alt="Alternate Text" /></aside>
    <h2>Acreditación por ICACIT</h2>
    <p>El programa de Ingeniería de Sistemas de la Universidad Andina del Cusco está acreditado por el Comité de Acreditación de Computación de ICACIT
http://www.icacit.org.pe

La Escuela Profesional de Ingeniería de Sistemas de la Universidad Andina del Cusco se encuentra acreditada bajo el modelo del Instituto de Calidad y Acreditación de Programas de Computación, Ingeniería y Tecnología (ICACIT). La acreditación, otorgada en 2019 por un período de seis años, reconoce el logro de los más altos estándares de calidad educativa y demuestra nuestro compromiso con la excelencia en la formación de Ingenieros de Sistemas.</p>
    <h3>SOBRE ICACIT</h3>
    <p>El Instituto de Calidad y Acreditación de Programas de Computación, Ingeniería y Tecnología es una agencia acreditadora especializada en programas de formación profesional en computación, ingeniería y tecnología en ingeniería. ICACIT fomenta la mejora continua de la calidad educativa, garantizando que lo programas cumplan con los más altos estándares internacionales.

ICACIT es integrante del Washington Accord, un acuerdo entre las más prestigiosas agencias de acreditación de ingeniería en el mundo. En el Perú, ICACIT está conformada por cinco sociedades profesionales y empresariales: Colegio de Ingenieros del Perú (CIP), Confederación Nacional de Instituciones Empresariales Privadas (CONFIEP), Asociación Peruana de Desarrolladores de Software (APESOF), Academia Peruana de Ingeniería (API), y Sección Perú del IEEE.</p>
        <img src="https://www.uandina.edu.pe/wp-content/uploads/2021/05/icacit-300x93.jpg" alt="Alternate Text" />

</asp:Content>
