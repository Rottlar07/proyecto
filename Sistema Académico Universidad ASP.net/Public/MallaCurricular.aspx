﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Public/Site1.Master" AutoEventWireup="true" CodeBehind="MallaCurricular.aspx.cs" Inherits="Sistema_Académico_Universidad_ASP.net.Public.WebForm3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3>DURACIÓN</h3>
    <p>10 ciclos académicos</p>
    <ul>
        <li>El plan de estudios se desarrolla en diez semestres académicos distribuidos en cinco años.</li>
    </ul>
    <h3>CERTIFICACIONES INTERMEDIAS</h3>
    <ul>
        <li>6to semestre: Analista de Sistemas.</li>
        <li>9no semestre: Administrador de Redes.</li>
    </ul>
    <h3>GRADO ACADÉMICO</h3>
    <ul>
        <li>Bachiller en Ingeniería de Sistemas</li>
    </ul>
    <h3>TÍTULO PROFESIONAL</h3>
    <ul>
        <li>Ingeniero de Sistemas</li>
    </ul>
    <h3>MALLA CURRICULAR</h3>
    <h2>2016</h2>
    <div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<table style="color: #5e6a78; letter-spacing: -0.1px;" border="1" width="100%" align="center">
<thead>
<tr style="background-color: #194d84; font-weight: bold; color: white;">
<th style="text-align: center;" scope="row" width="10%"><b>01</b></th>
<th style="text-align: center;" scope="row" width="10%"><b>02</b></th>
<th style="text-align: center;" scope="row" width="10%"><b>03</b></th>
<th style="text-align: center;" scope="row" width="10%"><b>04</b></th>
<th style="text-align: center;" scope="row" width="10%"><b>05</b></th>
<th style="text-align: center;" scope="row" width="10%"><b>06</b></th>
<th style="text-align: center;" scope="row" width="10%"><b>07</b></th>
<th style="text-align: center;" scope="row" width="10%"><b>08</b></th>
<th style="text-align: center;" scope="row" width="10%"><b>09</b></th>
<th style="text-align: center;" scope="row" width="10%"><b>10</b></th>
</tr>
</thead>
<tbody>
<tr style="height: 30px;">
<td style="background-color: #d6f1ff; padding-left: 10px; font-size: small;">Antropología, Hombre, Cultura y Sociedad</td>
<td style="background-color: #d6f1ff; padding-left: 10px; font-size: small;">Realidad Nacional y Globalización</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Algorítmica y Laboratorio de Programación I</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Algorítmica y Laboratorio de Programación II</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Desarrollo de Software I</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Desarrollo de Software II</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Desarrollo de Software III</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Desarrollo de Plataformas de Software</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Deontología Profesional en Ingeniería de Sistemas</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Tópicos de Ciencia de Datos</td>
</tr>
<tr style="height: 30px;">
<td style="background-color: #d6f1ff; padding-left: 10px; font-size: small;">Matemática I</td>
<td style="background-color: #d6f1ff; padding-left: 10px; font-size: small;">Pensamiento Filosófico, Ética y Ciudadanía</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Matemática Discreta</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Estructura de Datos</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Sistemas de Base de Datos I</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Sistemas de Base de Datos II</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Métodos Numéricos para la Computación</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Modelación y Simulación de Sistemas</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Seguridad de Tecnologías de Información y Comunicación</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Control y Auditoría de Sistemas</td>
</tr>
<tr style="height: 30px;">
<td style="background-color: #d6f1ff; padding-left: 10px; font-size: small;">Seminario Taller de Comunicación Oral y Escrita</td>
<td style="background-color: #d6f1ff; padding-left: 10px; font-size: small;">Matemática II</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Cálculo I</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Cálculo II</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Cálculo III</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Investigación Operativa II</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Modelos Estocásticos</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Metodología de la Investigación</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Dinámica de Sistemas</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Seminario Taller de Tesis II</td>
</tr>
<tr style="height: 30px;">
<td style="background-color: #d6f1ff; padding-left: 10px; font-size: small;">Técnicas de Estudio y de Investigación</td>
<td style="background-color: #d6f1ff; padding-left: 10px; font-size: small;">Seminario Taller de Desarrollo Personal</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Física I</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Álgebra Lineal</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Investigación Operativa I</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Sistemas Embebidos</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Sistemas Operativos</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Redes y Comunicaciones de Datos I</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Seminario Taller de Tesis I</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Gestión de la Innovación y Emprendimiento de Sistemas de Información</td>
</tr>
<tr style="height: 30px;">
<td style="background-color: #d6f1ff; padding-left: 10px; font-size: small;">Ecología y Medio Ambiente</td>
<td style="background-color: #d6f1ff; padding-left: 10px; font-size: small;">Actividades Extracurriculares</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Estadística II</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Fundamentos de Electricidad y Electrónica</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Sistemas Digitales</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Arquitectura del Computador</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Fundamentos de Bioinformática</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Formulación de Proyectos de Sistemas</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Redes y Comunicaciones de Datos II</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Ingeniería de la Información</td>
</tr>
<tr style="height: 30px;">
<td style="background-color: #d6f1ff; padding-left: 10px; font-size: small;">Seminario Taller de Tecnologías de Información y Comunicación</td>
<td style="background-color: #d6f1ff; padding-left: 10px; font-size: small;">Estadística I</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Gestión de Proceso de Negocio</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Análisis y Diseño de Sistemas de Información I</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Análisis y Diseño de Sistemas de Información II</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Gestión Estratégica de Tecnología de Información</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Ingeniería Económica</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Electivo I</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Gestión de Proyectos de Sistemas</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Electivo III</td>
</tr>
<tr style="height: 30px;">
<td style="background-color: #d6f1ff; padding-left: 10px; font-size: small;">Lengua Nativa: Quechua I</td>
<td style="background-color: #d6f1ff; padding-left: 10px; font-size: small;">Lengua Nativa: Quechua II</td>
<td style="padding-left: 10px; font-size: small;"></td>
<td style="padding-left: 10px; font-size: small;"></td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Constitución Política del Perú y Derechos Humanos</td>
<td style="padding-left: 10px; font-size: small;"></td>
<td style="padding-left: 10px; font-size: small;"></td>
<td style="padding-left: 10px; font-size: small;"></td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Electivo II</td>
<td style="padding-left: 10px; font-size: small;"></td>
</tr>
<tr style="height: 30px;">
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Introducción a la Ingeniería de Sistemas</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Tópicos de Ingeniería de Sistemas de Información</td>
<td style="padding-left: 10px; font-size: small;"></td>
<td style="padding-left: 10px; font-size: small;"></td>
<td style="padding-left: 10px; font-size: small;"></td>
<td style="padding-left: 10px; font-size: small;"></td>
<td style="padding-left: 10px; font-size: small;"></td>
<td style="padding-left: 10px; font-size: small;"></td>
<td style="padding-left: 10px; font-size: small;"></td>
<td style="padding-left: 10px; font-size: small;"></td>
</tr>
</tbody>
</table>

		</div>
	</div>
    <h2>2020</h2>
    <div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<table style="color: #5e6a78; letter-spacing: -0.1px;" border="1" width="100%" align="center">
<thead>
<tr style="background-color: #194d84; font-weight: bold; color: white;">
<th style="text-align: center;" scope="row" width="10%"><b>01</b></th>
<th style="text-align: center;" scope="row" width="10%"><b>02</b></th>
<th style="text-align: center;" scope="row" width="10%"><b>03</b></th>
<th style="text-align: center;" scope="row" width="10%"><b>04</b></th>
<th style="text-align: center;" scope="row" width="10%"><b>05</b></th>
<th style="text-align: center;" scope="row" width="10%"><b>06</b></th>
<th style="text-align: center;" scope="row" width="10%"><b>07</b></th>
<th style="text-align: center;" scope="row" width="10%"><b>08</b></th>
<th style="text-align: center;" scope="row" width="10%"><b>09</b></th>
<th style="text-align: center;" scope="row" width="10%"><b>10</b></th>
</tr>
</thead>
<tbody>
<tr style="height: 30px;">
<td style="background-color: #d6f1ff; padding-left: 10px; font-size: small;">Antropología, Hombre, Cultura y Sociedad</td>
<td style="background-color: #d6f1ff; padding-left: 10px; font-size: small;">Realidad Nacional y Globalización</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Pensamiento Filosófico y Ciudadanía</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Ingeniería de Costos</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Ética y Deontología en Ingeniería de Sistemas</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Investigación Operativa II</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Ingeniería Económica</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Formulación de Proyectos de Sistemas</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Gestión de la Innovación y Emprendimiento</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Inteligencia Artificial</td>
</tr>
<tr style="height: 30px;">
<td style="background-color: #d6f1ff; padding-left: 10px; font-size: small;">Medio Ambiente y Desarrollo Sostenible</td>
<td style="background-color: #d6f1ff; padding-left: 10px; font-size: small;">Seminario Taller de Desarrollo Personal</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Cálculo III</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Cálculo III</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Investigación Operativa I</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Organización y Arquitectura del Computador</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Arquitectura Empresarial</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Infraestructura de Redes</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Inteligencia de Negocios</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Seguridad en Tecnologías de Información</td>
</tr>
<tr style="height: 30px;">
<td style="background-color: #d6f1ff; padding-left: 10px; font-size: small;">Pre Cálculo</td>
<td style="background-color: #d6f1ff; padding-left: 10px; font-size: small;">Cálculo I</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Física I</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Física II</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Física III</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Matemática Discreta II</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Sistemas Operativos</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Plataformas para el Desarrollo de Aplicaciones</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Administración de Servicios de Redes</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Control y Auditoría de Sistemas</td>
</tr>
<tr style="height: 30px;">
<td style="background-color: #d6f1ff; padding-left: 10px; font-size: small;">Seminario Taller de Comunicación Oral y Escrita</td>
<td style="background-color: #d6f1ff; padding-left: 10px; font-size: small;">Estadística I</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Estadística II</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Álgebra Lineal</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Matemática Discreta I</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Desarrollo de Software I</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Desarrollo de Software I</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Metodología de Ia Investigación en Ingeniería</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Seminario Taller de Tesis I</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Seminario Taller de Tesis II</td>
</tr>
<tr style="height: 30px;">
<td style="background-color: #d6f1ff; padding-left: 10px; font-size: small;">Taller de Lengua Nativa: Quechua</td>
<td style="background-color: #d6f1ff; padding-left: 10px; font-size: small;">Taller de Tecnologías de Información y Comunicación</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Biología</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Estructura de Datos</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Modelado de Base de Datos</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Administración y Organización de Base de Datos</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Gestión Estratégica de Tecnología de Información</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Ingeniería de Software</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Sistemas de Información Gerencial</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Calidad de Sistemas de Información</td>
</tr>
<tr style="height: 30px;">
<td style="background-color: #d6f1ff; padding-left: 10px; font-size: small;">Métodos y Técnicas de Estudio Universitario</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Algorítmica de Lenguaje de Programación Estructurada</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Algorítmica de Lenguaje de Programación Orientada a Objetos</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Organización y Administración de Sistemas de Información</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Gestión de Procesos de Negocio</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Análisis y Diseño de Sistemas de Información I</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Análisis y Diseño de Sistemas de Información II</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Electivo I</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Electivo II</td>
<td style="background-color: #fbe4b1; padding-left: 10px; font-size: small;">Electivo III</td>
</tr>
<tr style="height: 30px;">
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Introducción a la Ingeniería de Sistemas</td>
<td style="background-color: #d2f3ce; padding-left: 10px; font-size: small;">Ingeniería de Sistemas</td>
<td style="padding-left: 10px; font-size: small;"></td>
<td style="padding-left: 10px; font-size: small;"></td>
<td style="padding-left: 10px; font-size: small;"></td>
<td style="padding-left: 10px; font-size: small;"></td>
<td style="padding-left: 10px; font-size: small;"></td>
<td style="padding-left: 10px; font-size: small;"></td>
<td style="padding-left: 10px; font-size: small;"></td>
<td style="padding-left: 10px; font-size: small;"></td>
</tr>
</tbody>
</table>

		</div>
	</div>
</asp:Content>
