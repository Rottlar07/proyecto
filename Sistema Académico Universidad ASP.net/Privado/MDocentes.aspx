﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Public/Site1.Master" AutoEventWireup="true" CodeBehind="MDocentes.aspx.cs" Inherits="Sistema_Académico_Universidad_ASP.net.WebForm1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <p>CodDocente:<asp:TextBox runat="server" id="txtId"/></p>
        <p>APaterno:<asp:TextBox runat="server" id="txtAPaterno"/></p>
        <p>AMaterno:<asp:TextBox runat="server" id="txtAMaterno"/></p>
        <p>Nombres:<asp:TextBox runat="server" id="txtNombres"/></p>
        <p>Usuario:<asp:TextBox runat="server" id="txtUsuario"/></p>

        <p>
            <asp:Button Text="Agregar" runat="server" id="btnAgregar" OnClick="btnAgregar_Click"/>
            <asp:Button Text="Actualizar" runat="server" id="btnActualizar" OnClick="btnActualizar_Click"/>
            <asp:Button Text="Eliminar" runat="server" id="btnEliminar" OnClick="btnEliminar_Click"/>
            <asp:Button Text="Buscar" runat="server" id="btnBuscar" OnClick="btnBuscar_Click"/>
        </p>

        <p>
            <asp:GridView runat="server" ID="gvEjemplo" BackColor="White" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px" CellPadding="4" GridLines="Horizontal">
                <FooterStyle BackColor="White" ForeColor="#333333" />
                <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="White" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                <SortedAscendingHeaderStyle BackColor="#487575" />
                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                <SortedDescendingHeaderStyle BackColor="#275353" />
            </asp:GridView>
        </p>
        <p>
            Usted ingresó como: <asp:LoginName ID="LoginName1" runat="server" />
            <br />
            <asp:LoginStatus ID="LoginStatus1" runat="server" />
        </p>
        
    </div>
</asp:Content>
