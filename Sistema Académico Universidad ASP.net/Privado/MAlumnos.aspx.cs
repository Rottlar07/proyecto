﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Configuration;
using System.Data;
using MySql.Data.MySqlClient;

namespace Sistema_Académico_Universidad_ASP.net.Public
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        private static string cadena = ConfigurationManager.ConnectionStrings["Cadena"].ConnectionString;
        private static MySqlConnection conexion = new MySqlConnection(cadena);

        private void Listar()
        {
            string consulta = "select * from talumno";
            MySqlCommand comando = new MySqlCommand(consulta, conexion);
            MySqlDataAdapter adaptador = new MySqlDataAdapter(comando);
            DataTable tabla = new DataTable();
            adaptador.Fill(tabla);
            gvEjemplo.DataSource = tabla;
            gvEjemplo.DataBind();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Listar();
            }
        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                string id = txtId.Text.Trim();
                string AP = txtAPaterno.Text.Trim();
                string AM = txtAMaterno.Text.Trim();
                string Nom = txtNombres.Text.Trim();
                string User = txtUsuario.Text.Trim();
                string CodCar = txtCodCarrera.Text.Trim();

                string consulta = "insert into talumno values('" + id + "','" + AP + "','" + AM + "','" + Nom + "','" + User + "','" + CodCar + "')";
                MySqlCommand comando = new MySqlCommand(consulta, conexion);
                conexion.Open();
                int i = comando.ExecuteNonQuery();
                conexion.Close();
                if (i == 1)
                {
                    Listar();
                }
                else
                {
                    Response.Write("Error al insertar a la base de datos");
                }
            }catch(MySqlException exc)
            {
                conexion.Close();
                Response.Write(exc.Message);
            }
}

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                string id = txtId.Text.Trim();

                string consulta = "delete from talumno where CodAlumno='"+id+"'";
                MySqlCommand comando = new MySqlCommand(consulta, conexion);
                conexion.Open();
                int i = comando.ExecuteNonQuery();
                conexion.Close();
                if (i == 1)
                {
                    Listar();
                }
                else
                {
                    Response.Write("Error al eliminar registro");
                }
            }
            catch (MySqlException exc)
            {
                conexion.Close();
                Response.Write(exc.Message);
            }
        }

        protected void btnActualizar_Click(object sender, EventArgs e)
        {
            try
            {
                string id = txtId.Text.Trim();
                string AP = txtAPaterno.Text.Trim();
                string AM = txtAMaterno.Text.Trim();
                string Nom = txtNombres.Text.Trim();
                string User = txtUsuario.Text.Trim();
                string CodCar = txtCodCarrera.Text.Trim();

                string consulta = "update talumno set APaterno='"+AP+"',AMaterno='"+AM+"',Nombres='"+Nom+"',Usuario='"+User+"',CodCarrera='"+CodCar+"' where CodAlumno='"+id+"'";
                MySqlCommand comando = new MySqlCommand(consulta, conexion);
                conexion.Open();
                int i = comando.ExecuteNonQuery();
                conexion.Close();
                if (i == 1)
                {
                    Listar();
                }
                else
                {
                    Response.Write("Error al actualizar la base de datos");
                }
            }
            catch (MySqlException exc)
            {
                conexion.Close();
                Response.Write(exc.Message);
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            string id = txtId.Text.Trim();
            string consulta = "select * from talumno where CodAlumno='" + id + "'";
            MySqlCommand comando = new MySqlCommand(consulta, conexion);
            MySqlDataAdapter adaptador = new MySqlDataAdapter(comando);
            DataTable tabla = new DataTable();
            adaptador.Fill(tabla);
            gvEjemplo.DataSource = tabla;
            gvEjemplo.DataBind();
        }
    }
}