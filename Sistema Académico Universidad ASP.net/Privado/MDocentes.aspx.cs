﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Configuration;
using System.Data;
using MySql.Data.MySqlClient;

namespace Sistema_Académico_Universidad_ASP.net
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        private static string cadena = ConfigurationManager.ConnectionStrings["Cadena"].ConnectionString;
        private static MySqlConnection conexion = new MySqlConnection(cadena);

        private void Listar()
        {
            string consulta = "select * from tdocente";
            MySqlCommand comando = new MySqlCommand(consulta, conexion);
            MySqlDataAdapter adaptador = new MySqlDataAdapter(comando);
            DataTable tabla = new DataTable();
            adaptador.Fill(tabla);
            gvEjemplo.DataSource = tabla;
            gvEjemplo.DataBind();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Listar();
            }
        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                string id = txtId.Text.Trim();
                string AP = txtAPaterno.Text.Trim();
                string AM = txtAMaterno.Text.Trim();
                string nom = txtNombres.Text.Trim();
                string user = txtUsuario.Text.Trim();

                string consulta = "insert into tdocente values('" + id + "','" + AP + "','" + AM + "','" + nom + "','" + user + "')";
                MySqlCommand comando = new MySqlCommand(consulta, conexion);
                conexion.Open();
                int i = comando.ExecuteNonQuery();
                conexion.Close();
                if (i == 1)
                {
                    Listar();
                }
                else
                {
                    Response.Write("Error al insertar a la base de datos");
                }
            }catch(MySqlException exc)
            {
                conexion.Close();
                Response.Write(exc.Message);
            }
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                string id = txtId.Text.Trim();

                string consulta = "delete from tcarga where CodDocente='" + id + "';delete from tdocente where CodDocente='" + id + "'";
                MySqlCommand comando = new MySqlCommand(consulta, conexion);
                conexion.Open();
                int i = comando.ExecuteNonQuery();
                conexion.Close();
                if (i >= 1)
                {
                    Listar();
                }
                else
                {
                    Response.Write("Error al eliminar registro");
                }
            }
            catch (MySqlException exc)
            {
                conexion.Close();
                Response.Write(exc.Message);
            }
        }

        protected void btnActualizar_Click(object sender, EventArgs e)
        {
            try
            {
                string id = txtId.Text.Trim();
                string AP = txtAPaterno.Text.Trim();
                string AM = txtAMaterno.Text.Trim();
                string nom = txtNombres.Text.Trim();
                string user = txtUsuario.Text.Trim();

                string consulta = "update tcarga set CodDocente='"+id+ "' where CodDocente='"+id+"';update tdocente set APaterno='" + AP+"',AMaterno='"+AM+"',Nombres='"+nom+"',Usuario='"+user+"' where CodDocente='"+id+"'";
                MySqlCommand comando = new MySqlCommand(consulta, conexion);
                conexion.Open();
                int i = comando.ExecuteNonQuery();
                conexion.Close();
                if (i >= 1)
                {
                    Listar();
                }
                else
                {
                    Response.Write("Error al insertar a la base de datos");
                }
            }
            catch (MySqlException exc)
            {
                conexion.Close();
                Response.Write(exc.Message);
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            string id = txtId.Text.Trim();
            string consulta = "select * from tdocente where CodDocente='" + id + "'";
            MySqlCommand comando = new MySqlCommand(consulta, conexion);
            MySqlDataAdapter adaptador = new MySqlDataAdapter(comando);
            DataTable tabla = new DataTable();
            adaptador.Fill(tabla);
            gvEjemplo.DataSource = tabla;
            gvEjemplo.DataBind();
        }
    }
}